@extends('adminlte::page')

@section('title', 'Cadastro de Carros')

@section('content_header')
    <h1>Gráfico do cadastro de Carros
    <a href="{{ route('carros.index') }}" 
       class="btn btn-primary pull-right" role="button">Listagem</a>
    </h1>
@endsection

@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Marcas', 'Número de veículos'],
          //!! faz com que damos um echo para mostrar as linhas desejadas na tela

        @foreach ($dados as $linha)
        
            {!! "['$linha->marca',$linha->num]," !!}

        @endforeach
        ]);

        var options = {
          title: 'Carros por marca',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>

    <div id="donutchart" style="width: 900px; height: 500px;"></div>


@endsection