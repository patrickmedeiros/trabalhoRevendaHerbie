@extends('site.modelo')

@section('conteudo')


    <div class="container">
        <h2>Envie sua proposta</h2>
  

        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <form method="post" class="form-group" action="{{route('proposta.store')}}">
                    {{ csrf_field() }}

                    <label for="nome" class="mr-sm-2">Nome: </label>
                    <input type="text" id="nome" name="nome" class="form-control" required>

                    <label for="email" class="mr-sm-2">E-mail: </label>
                    <input type="email" id="email" name="email" class="form-control" required>

                    <label for="telefone" class="mr-sm-2">Fone: </label>
                    <input type="text" id="telefone" name="telefone" class="form-control" required>

                    <label for="data" class="mr-sm-2">Data : </label>
                    <input type="date" id="data" name="data" class="form-control" required>

                    <label for="proposta" class="mr-sm-2">Valor da proposta: </label>
                    <input type="text" id="proposta" name="proposta" class="form-control" required>

                    <br>
                    <input type="hidden" name="veiculo_id" value="{{$reg->id}}">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <input type="reset" value="Limpar" class="btn btn-warning">
                    </form>
                </div>

                <div class="col-sm-4">
                @if(Storage::exists($reg->foto))
                    <img src="{{url('storage/'.$reg->foto)}}"
                        style="width: 100%; height: 150px;" 
                        alt="Foto de Carro"/>
                @else
                    <img src="{{url('storage/fotos/sem_foto.png')}}"
                        style="width: 100%; height: 150px;" 
                        alt="Foto de Carro"/>
                @endif
                <ul class="none">
                    <li><b><i class="fas fa-car"></i></b> {{$reg->modelo}}</li>
                    <li><b> <i class="far fa-calendar-alt"></i></b> {{$reg->ano}}</li>
                    <li><b><i class="fas fa-ticket-alt"></i></b> {{$reg->marca->nome}}</li>
                    <li><b><i class="fas fa-gas-pump"></i></b> {{$reg->combustivel}}</li>
                    <li><h2><i class="fas fa-dollar-sign"></i>{{ number_format($reg->preco, '2', ',', '.')}}</h2></li>
                </ul>
            </div>
        </div>
    </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.js"></script>
        <script>
            $(document).ready(function () {
                $('#proposta').mask('0.000.000,00', {reverse: true});
                $('#telefone').mask('(00) 0 0000-0000');
                            });
        </script>
@endsection