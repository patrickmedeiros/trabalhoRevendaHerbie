<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Revenda Herbie</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>



<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('inicio.index') }}">Início</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('principal.index') }}">Carros</a>
    </li>
  
    <form class="form-inline" role="search" action="{{ route('inicio.pesquisar') }}" method="post" style="margin-left: 200%;">
    {!! csrf_field() !!}
      <input class="form-control mr-sm-2" type="text" name="modelo" id="modelo" placeholder="Buscar carros">
    </form>

  </ul>
</nav>

@yield('conteudo')
</body>
</html>