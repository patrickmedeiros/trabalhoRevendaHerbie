@extends('site.modelo')

@section('conteudo')
<br>
<div class="container">
<div class="row">
@forelse($carros as $c)
        <div class="col col-sm-4">
            <div class="card border-primary bg-light">
                <div class="card-header text-center" style="font-weight: bold;">{{ $c->modelo }}</div>
                    <div class="card-body">
                        @if(Storage::exists($c->foto))
                            <img src="{{url('storage/'.$c->foto)}}"
                            style="width: 100%; height: 150px;" 
                            alt="Foto de Carro"/>
                        @else
                            <img src="{{url('storage/fotos/sem_foto.png')}}"
                            style="width: 100%; height: 150px;" 
                            alt="Foto de Carro"/>
                        @endif
                        <hr>
                        <p class="text-center">Valor R$: {{number_format($c->preco, 2, ',', '.')}} </p>
                        <a href="{{ route('proposta.index') }}/{{ $c->id }}" class="btn btn-block btn-outline-dark" role="button">Proposta</a>
                    </div>
                </div>
                <br>
        </div>

@empty
        <h4>Não existe(m) carro(s) em destaque no momento !</h4>

@endforelse
</div>
</div>
@endsection
