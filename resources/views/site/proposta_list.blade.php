@extends('adminlte::page')

@section('title', 'Lista de propostas')

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

<table class="table table-striped">
  <tr>
    <th> Nome </th>
    <th> E-mail </th>
    <th> Telefone </th>
    <th> Proposta </th>
    <th> Data </th>
    <th> ID do Veículo </th>
    <th> Ações </th>
  </tr>  
@forelse($propostas as $c)
  <tr>
    <td> {{$c->nome}} </td>
    <td> {{$c->email}} </td>
    <td> {{$c->telefone}} </td>
    <td> {{$c->proposta}} </td>
    <td> {{$c->data}} </td>
    <td> {{$c->veiculo_id}} </td>
   </td>
    <td> 
        <form style="display: inline-block"
              method="post"
              action="{{route('proposta.destroy', $c->id)}}"
              onsubmit="return confirm('Confirma Exclusão?')">
               {{method_field('delete')}}
               {{csrf_field()}}
              <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>&nbsp;&nbsp;
    </td>
  </tr>

@empty
  <tr><td colspan=8> Não há propostas cadastradas ou filtro da pesquisa não 
                     encontrou registros </td></tr>
@endforelse
</table>

@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection