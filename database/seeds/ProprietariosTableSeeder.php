<?php

use Illuminate\Database\Seeder;

class ProprietariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proprietarios')->insert([
            'nome' => 'Rodrigo',
            'email' => 'rodrigo@rodrigo.com.br',
            'telefone' => '53 9 8345-3421',          
            'cidade' => 'Pelotas',          
        ]);
        DB::table('proprietarios')->insert([
            'nome' => 'Gabriel',
            'email' => 'gabriel@gabriel.com.br',
            'telefone' => '53 9 9872-2345',          
            'cidade' => 'Caçapava',          
        ]);
        DB::table('proprietarios')->insert([
            'nome' => 'Luka',
            'email' => 'luka@luka.com.br',
            'telefone' => '53 9 4567-5432',          
            'cidade' => 'Pelotas',          
        ]);
        DB::table('proprietarios')->insert([
            'nome' => 'Patrick',
            'email' => 'patrick@patrick.com.br',
            'telefone' => '53 9 9872-9827',          
            'cidade' => 'Jaguarão',          
        ]);
        DB::table('proprietarios')->insert([
            'nome' => 'Edécio',
            'email' => 'edecio@edecio.com.br',
            'telefone' => '53 9 8765-9879',          
            'cidade' => 'Pelotas',          
        ]);
    }
}
