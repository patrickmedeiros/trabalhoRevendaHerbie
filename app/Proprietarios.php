<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proprietarios extends Model
{
    protected $fillable = ['nome', 'email', 'telefone', 'cidade'];
}
