<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carro;

class wsController extends Controller
{
    public function wsCarro($id=null){
        //indica o tipo de retorno do método
        header("Content-type: application/json; carset=utf-8");

        //verifica se id foi (ou não) passado
        if($id==null){
            $retorno = array("status" => "url incorreta",
                            "modelo" => null,
                            "ano" => null,
                            "preco" => null);
        }else{
            //busca o registro
            $reg = Carro::find($id);

            //se encontrado
            if(isset($reg)){
                $retorno = array("status" => "encontrado",
                            "modelo" => $reg->modelo,
                            "ano" => $reg->ano,
                            "preco" => $reg->preco);
            }else{
                $retorno = array("status" => "inexistente",
                            "modelo" => null,
                            "ano" => null,
                            "preco" => null);
            }
        }
        // converte array para formato json
        echo json_encode($retorno, JSON_PRETTY_PRINT);
    }
    public function wsxml($id = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        //adiciona carros ao XML
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><carros></carros>');

        //verifica se $id não foi passado
        if ($id == null){
            //seria um filho do carros que estamos buscando na linha de cima
            $item = $xml->addChild('carro');
            //atributos deste registro
            $item -> addChild("status","url incorreta");
            $item -> addChild("modelo",null);
            $item -> addChild("ano",null);
            $item -> addChild("preco",null);
        }else {
            //busca o veículo cujo id foi passado como parâmetro
            $reg = Carro::find($id);
            
            //se existe
            if(isset($reg)){
                $item = $xml->addChild('carro');
                $item -> addChild("status","Encontrado");
                $item -> addChild("modelo","$reg->modelo");
                $item -> addChild("ano","$reg->ano");
                $item -> addChild("preco","$reg->preco");
            }else {
                $item = $xml->addChild('carro');
                $item -> addChild("status","Inexistente");
                $item -> addChild("modelo",null);
                $item -> addChild("ano",null);
                $item -> addChild("preco",null);
            }
        }
        //retorna os dados no format XML
        echo $xml->asXML();
    }

    public function listaxml($preco = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        //adiciona carros ao XML
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><carros></carros>');

        //verifica se $id não foi passado
        if ($preco == null){
            //seria um filho do carros que estamos buscando na linha de cima
            $item = $xml->addChild('carro');
            //atributos deste registro
            $item -> addChild("status","url incorreta");
            $item -> addChild("modelo",null);
            $item -> addChild("ano",null);
            $item -> addChild("preco",null);
        }else {
            //busca os veículo cujo preco seja menos que $preco
            $carros = Carro::where("preco","<=",$preco)->get();
            
            //se existe
            if(count($carros) > 0){
                foreach($carros as $c){
                $item = $xml->addChild('carro');
                $item -> addChild("status","Encontrado");
                $item -> addChild("modelo","$c->modelo");
                $item -> addChild("ano","$c->ano");
                $item -> addChild("preco","$c->preco");
                }
            }else {
                $item = $xml->addChild('carro');
                $item -> addChild("status","Inexistente");
                $item -> addChild("modelo",null);
                $item -> addChild("ano",null);
                $item -> addChild("preco",null);
            }
        }
        //retorna os dados no format XML
        echo $xml->asXML();
    }
}