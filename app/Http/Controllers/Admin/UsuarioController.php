<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;

class UsuarioController extends Controller
{
    
    public function index()
    {
        $dados = User::all(); 


        return view('admin.usuario_list', ['usuario' => $dados]);
    }
    
}
