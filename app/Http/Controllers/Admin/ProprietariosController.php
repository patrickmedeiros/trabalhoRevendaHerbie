<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Carro;
use App\Marca;
use App\Proprietarios;

class ProprietariosController extends Controller
{
    public function index(){

        $dados = Proprietarios::all();
        return view('admin.proprietarios_lista', ['proprietarios' => $dados]);
            

    }
    public function show($id) {

   }

   public function create() {

    return view('admin.proprietarios_form', ['acao' => 1]);
       
   }

   public function store(Request $request) {

    $this->validate($request, [
        'nome' => 'required|min:2|max:200',
        'email' => 'required',
        'telefone' => 'min:9|max:40'
    ]);
        
    Proprietarios::create($request->all());

    return redirect()->route('proprietarios.index')
        ->with('status','Proprietário incluído com sucesso!');

   }

   public function edit($id) {
       // posiciona no registro a ser alterado e obtém seus dados
       $reg = Proprietarios::find($id);
       
       return view('admin.proprietarios_form', ['reg' => $reg, 'acao' => 2]);
   

    }

   public function update(Request $request, $id) {

        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Proprietarios::find($id);

         // realiza a alteração
         $alt = $reg->update($dados);
        
        if ($alt) {
            return redirect()->route('proprietarios.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
       
   }

   public function destroy($id){

        $prop = Proprietarios::find($id);

        if ($prop->delete()) {
            return redirect()->route('proprietarios.index')
                    ->with('status', 'Proprietário ' . $prop->nome . ' excluído!');
        }   
   }
   public function wsProprietarios($id=null){
    //indica o tipo de retorno do método
    header("Content-type: application/json; carset=utf-8");

    //verifica se id foi (ou não) passado
    if($id==null){
        $retorno = array("status" => "url incorreta",
                        "nome" => null,
                        "email" => null,
                        "telefone" => null,
                        "cidade" => null);
    }else{
        //busca o registro
        $reg = Proprietarios::find($id);

        //se encontrado
        if(isset($reg)){
            $retorno = array("status" => "encontrado",
                        "nome" => $reg->nome,
                        "email" => $reg->email,
                        "telefone" => $reg->telefone,
                        "cidade" => $reg->cidade);
        }else{
            $retorno = array("status" => "inexistente",
                        "nome" => null,
                        "email" => null,
                        "telefone" => null,
                        "cidade" => null);
        }
    }
    // converte array para formato json
    echo json_encode($retorno, JSON_PRETTY_PRINT);
    }

    public function wsxmlProprietarios($id = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        //adiciona proprietarios ao XML
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><proprietarios></proprietarios>');

        //verifica se $id não foi passado
        if ($id == null){
            //seria um filho do proprietarios que estamos buscando na linha de cima
            $item = $xml->addChild('proprietario');
            //atributos deste registro
            $item -> addChild("status","url incorreta");
            $item -> addChild("nome",null);
            $item -> addChild("email",null);
            $item -> addChild("telefone",null);
            $item -> addChild("cidade",null);
        }else {
            //busca o veículo cujo id foi passado como parâmetro
            $reg = Proprietarios::find($id);
            
            //se existe
            if(isset($reg)){
                $item = $xml->addChild('proprietario');
                $item -> addChild("status","Encontrado");
                $item -> addChild("nome","$reg->nome");
                $item -> addChild("email","$reg->email");
                $item -> addChild("telefone","$reg->telefone");
                $item -> addChild("cidade","$reg->cidade");
            }else {
                $item = $xml->addChild('proprietario');
                $item -> addChild("status","Inexistente");
                $item -> addChild("nome",null);
                $item -> addChild("email",null);
                $item -> addChild("telefone",null);
                $item -> addChild("cidade",null);
            }
        }
        //retorna os dados no format XML
        echo $xml->asXML();
    }

    public function relProprietarios(){
        $proprietarios = Proprietarios::all();

        //stream exibe o relatório, está lá na rota do PDF
        return \PDF::loadView('admin.relatorio_proprietarios',['proprietarios'=>$proprietarios])->stream();
    }
}
