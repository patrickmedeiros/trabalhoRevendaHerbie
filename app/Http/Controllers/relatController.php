<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carro;

class relatController extends Controller
{
    public function relcarros(){
        $carros= Carro::all();

        //stream exibe o relatório, está lá na rota do PDF
        return \PDF::loadView('admin.relcarros',['carros'=>$carros])->stream();
    }
}
