<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Carro;
use App\Marca;

class InicioController extends Controller
{
    public function index(){
            
            $dados = DB::select("SELECT * FROM carros WHERE destaque = '*'");
            return view('site.inicio_lista',['carros' => $dados]);

    }
    public function show($id) {

   }

   public function create() {
       
       
   }

   public function store(Request $request) {
       

   }

   public function edit($id) {
   

    }

   public function update(Request $request, $id) {
       
   }

   public function destroy($id){
   
   }

   public function pesquisar(request $request){
        $modelo = $request->modelo;
        $filtro = array();
        if(!empty($modelo)){
                array_push($filtro, array('modelo','like','%'.$modelo.'%'));
        }

        $carros = Carro::where($filtro)->orderBy('modelo')->paginate(5);
        return view('site.carros_pesquisa',compact('carros'));

       //$texto = Input::any('texto');
        //$sql="select * from carros where modelo like %".$texto."%";
        //$pesquisa = Produtos::where('modelo', 'like', '%'.$texto.'%');

        //$dados = DB::select($sql);

        //return view('inicio.pesquisar', ['dados'=>$dados]);
        //$user = DB::table('carros')->where('modelo', 'sandero')->first();
        
        //echo "haha";
        }
}
