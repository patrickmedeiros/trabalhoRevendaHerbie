<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Carro;
use App\Marca;
use App\Proposta;

class PropostaController extends Controller
{
    public function index(){

        $dados = Proposta::all();
        return view('site.proposta_list', ['propostas' => $dados]);

    }
    public function show($id) {
        $reg = Carro::find($id);
        return view('site.proposta_form', ['reg' => $reg]);

   }

   public function create() {
       
       
   }

   public function store(Request $request) {

    $this->validate($request, [
        'nome' => 'required|min:2|max:200',
        'email' => 'required',
        'telefone' => 'min:9|max:40'
    ]);
    $request['proposta'] = str_replace(',','.',str_replace('.','',$request['proposta']));

    //$request['data']=date_format('y-m-d',$request['data']);
    //$request['data'] = date_format("y-m-d",$request['data']);
    //$request['data'] = str_replace('/',str_replace('-',$request['data']));
        
    Proposta::create($request->all());

    return redirect()->route('principal.index')
        ->with('status','Proposta inserida com sucesso!');
       

   }

   public function edit($id) {
   

    }

   public function update(Request $request, $id) {
       
   }

   public function destroy($id){

    $prop = Proposta::find($id);

        if ($prop->delete()) {

            return redirect()->route('proposta.index')
                        ->with('status', 'Proposta de id '.$prop->id.' do cliente '.$prop->nome.' excluída!');
        }
    }
    
    public function graf(){

        $sql = "select concat(year(data), '-', month(data)) as mes,  
        count(*) as num 
        from propostas 
        group by concat(year(data), '-', month(data))";

                $dados = DB::select($sql);

        return view('admin.proposta_graf', ['dados' => $dados]);
    }
}
