<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Carro;
use App\Marca;

class PrincipalController extends Controller
{
    public function index(){

        $dados = Carro::all();
        return view('site.principal_lista', ['carros' => $dados]);
            

    }
    public function show($id) {

   }

   public function create() {
       
       
   }

   public function store(Request $request) {
       

   }

   public function edit($id) {
   

    }

   public function update(Request $request, $id) {
       
   }

   public function destroy($id){
   
   }
}