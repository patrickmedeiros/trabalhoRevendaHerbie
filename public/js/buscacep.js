// cria referencia aos elementos da página
var cep = document.getElementById("cep");
var endereco = document.getElementById("endereco");
var bairro = document.getElementById("bairro");
var cidade = document.getElementById("cidade");
var estado = document.getElementById("estado");


function buscarCep(){

    var url = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep.value + "&formato=json";
    fetch(url)
    .then((resp) => resp.json()) // Transform the data into json
    .then(function(data) {
        //pega o que vem do JSON e coloca nas nossas variáveis escolhidas
        endereco.value = data.tipo_logradouro + " " + data.logradouro;
        bairro.value = data.bairro;
        cidade.value = data.cidade;
        estado.value = data.uf;
      });
}
//quando sair do campo CEP ele busca o endereco e demais registros
// associa a um evento do elemento cep uma function
cep.addEventListener("blur", buscarCep);