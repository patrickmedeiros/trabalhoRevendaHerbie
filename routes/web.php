<?php

Route::get('/teste', function () {
    return view('welcome');
})->middleware('auth'); //tranca rota principal para não passar sem fazer login

Route::group(['prefix'=>'admin', 'namespace'=>'Admin','middleware' => 'auth'], function() {
   Route::get('/', function() { return view('admin.index'); });
   Route::resource('carros', 'CarroController');
   Route::resource('usuario', 'UsuarioController');
   Route::resource('proprietarios', 'ProprietariosController');
   Route::get('carrosgraf','CarroController@graf') -> name('carros.graf');
   Route::any('carrosbuscar','CarroController@buscar') -> name('carros.buscar');
   Route::get('carrosdestaque/{id}','CarroController@destaque') -> name('carros.destaque');
   Route::get('cadcli','ClienteController@form') -> name('cad.cli');
   Route::get('/wsproprietarios/{id?}','ProprietariosController@wsProprietarios');
   Route::get('/wsxmlproprietarios/{id?}','ProprietariosController@wsxmlProprietarios');
   Route::get('/relproprietarios','ProprietariosController@relProprietarios');
});

Route::group(['prefix'=>'site', 'namespace'=>'Site'], function() {
    Route::get('/', function() { return view('site.index'); });
    Route::resource('inicio', 'InicioController');
    Route::resource('principal', 'PrincipalController');
    Route::resource('proposta', 'PropostaController');
    Route::get('propostasgraf','PropostaController@graf') -> name('propostas.graf');
    Route::any('pesquisar','InicioController@pesquisar') -> name('inicio.pesquisar');

});


//Route::get('/admin', function() {
//    return view('admin.index');
//});
Auth::routes(); //após colocar o adminlte no prompt

Route::get('/home', 'HomeController@index')->name('home'); //carrega o index da rota adminlte (tela de login)
//id? quer dizer que ele aceita mesmo se não tiver nada no id
Route::get('/wscarro/{id?}','wsController@wsCarro');

Route::get('/wsxml/{id?}','wsController@wsxml');

Route::get('/listaxml/{preco?}','wsController@listaxml');

Route::get('/rel',function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/relcarros','relatController@relcarros');